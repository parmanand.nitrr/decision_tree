from sklearn import tree
import numpy as np
import graphviz
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
import os
os.environ["PATH"] += os.pathsep + r'C:\Program Files (x86)\Graphviz2.38\bin'

if __name__ == '__main__':
    M = np.genfromtxt('./monk/monks-1.train', missing_values=0, skip_header=0, delimiter=',', dtype=int)
    ytrn = M[:, 0]
    Xtrn = M[:, 1:]
    print("Size of training data", Xtrn.shape)
    M = np.genfromtxt('./monk/monks-1.test', missing_values=0, skip_header=0, delimiter=',', dtype=int)
    ytst = M[:, 0]
    Xtst = M[:, 1:]
    print("Size of testing data", Xtst.shape)
    model = tree.DecisionTreeClassifier()
    model = model.fit(Xtrn, ytrn)
    dot_data = tree.export_graphviz(model, out_file=None, filled=True,
                                    rounded=True, special_characters=True)
    graph = graphviz.Source(dot_data)
    graph.render("balanc-sci")
    y_hat = model.predict(Xtst)
    cm = confusion_matrix(ytst, y_hat)

    titles_options = [("Confusion matrix, without normalization", None),
                      ("Normalized confusion matrix", 'true')]
    for Monk1, normalize in titles_options:
        disp = plot_confusion_matrix(model, Xtst, ytst,
                                     cmap=plt.cm.Blues,
                                     normalize=normalize)
        disp.ax_.set_title(Monk1)

        print(Monk1)
        print(disp.confusion_matrix)
    plt.show()
